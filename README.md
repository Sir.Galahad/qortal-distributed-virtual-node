# Qortal Distributed Virtual Node WIP

An extremely prototype software that takes advantage of the underlying utility of QChat as a means to move data instead of messages.

This currently doesn't have so much for outside usability and will only receive requests, query a node, then return. But doesn't provide any functionality to create any encoded messages or any way to use the enternal builders and SendAndWait methods from outside of itself.

This currently is set up to request and return data from the API of a Bitcoin clone.

The underlying message encoding scheme is a smol language I created called QByteLang that is intended to be a means of describing a simple instruction and how to act on that instruction.  

## How messages are encoded

All messages are encoded to fit within the ChatTx data size of 256 bytes.

For the uses of Bitcoin clone data, QChat messages are encoded in the following ways as well as the number of bytes each object should take up.

```
request: reference(4) version(1) processType(1) jobHash(4) CoinCode(2) requestType(1) DataModifier(1) data
        
returnPart: reference(4) version(1) processType(1) jobHash(4) CompleteDataHash(4) DataPartHash(4) PartNum(1) PartCount(1) Data
```

The first four bytes of the encoded data should be the first four bytes of the reference that is used to send the QChatTx. This is to show that the message is meant to be decodable.  

The next byte is the qByteVersion(currently 1) and is for the future separation of compatibility.

The next byte is the process type. currently either DataRequest or DataReturn and describes the action to be performed.

The next four bytes are the original request's JobHash and are the first four bytes of a SHA256 hash of the QByteLang byteCode after the first four bytes(because we don't know the transaction reference until we are sending on the network.)

### For Request:  
The next two bytes are a coinCode for the coin that the data should be from.  

The next byte is a request type that represents what API call will be made to a node.

The next byte is a Data Modifier to specify the length of the remaining data. 

The remaining Data. (For the supported calls there shouldn't ever be a situation that overruns the max data size)

### For Return Parts:

The next four bytes are the first four bytes of a SHA256 hash of the complete to be returned data. (for validation sakes)  

The next four bytes are the first four bytes of a Sha256 hash of only the data that is returned in this part. (this is probably not needed because data integrity is probably handled above my head in the node software.)  

The next byte is the part number.

The next Byte is the part count.

The rest of the message space is to be filled with data.


## How It Works

The requester Creates a Request for data from a certain node type using a QbyteRequestBuilder.   

That is then encoded to a set of instructions in QByteLang and sent as a QChatTx.  

A responding node can then take this set of encoded instructions and use it to know how their node needs to be queried.  

A responding node then uses a QByteReturnBuilder to build the return data into a list of QByteReturnParts that are then encoded and returned using the chatTx.

Anyone listning, including the initial requester can then read the parts from the chat channel and reconstruct them into a QByteReturn to be able to use the data returned for that Job.

## Supported Calls

```
GetBlock ByHeight, ByHash

GetTransaction ByHash
```
#### Donate

BTC: 1GZ2qkr4cqTNtbYb7Q3HXr2m3FeDcrXk2y

LTC: MLpuVPabD11PrQ6KUm7RRND2FHGkU7qHZd

QORT: QXZPc1JxDRWq2ruxuhVzirLqeb16rjpCc3


##### Made with ❤️ by Drew
