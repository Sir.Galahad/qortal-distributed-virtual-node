﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QortalDistributedFederatedNode.Models;

namespace QortalDistributedFederatedNode.WalletHandles
{
    public class BtcNodeHandle
    {
        public string NodeIndicator { get; set; }

        public enum NodeAction
        {
            add,
            remove,
            onetry
        }

        public enum MethodName
        {
            addmultisigaddress,
            addnode,
            addredeemscript,
            backupwallet,

            //checkkernel,
            checkwallet,

            //createrawtransaction,
            decoderawtransaction,

            decodescript,
            dumpprivkey,
            dumpwallet,
            encryptwallet,
            getaccount,
            getaccountaddress,
            getaddednodeinfo,
            getaddressesbyaccount,
            getbalance,
            getbestblockhash,
            getblock,
            getblockbynumber,
            getblockcount,
            getblockhash,
            getblocktemplate,
            getcheckpoint,
            getconnectioncount,
            getdifficulty,
            getinfo,
            getmininginfo,
            getnettotals,
            getnewaddress,
            getnewpubkey,
            getpeerinfo,
            getrawmempool,
            getrawtransaction,
            getreceivedbyaccount,
            getreceivedbyaddress,
            getstakesubsidy,
            getstakinginfo,
            getsubsidy,
            gettransaction,
            getwork,
            getworkex,
            help,
            importprivkey,
            importwallet,
            keypoolrefill,
            listaccounts,
            listaddressgroupings,
            listreceivedbyaccount,
            listreceivedbyaddress,
            listsinceblock,
            listtransactions,
            listunspent,
            makekeypair,
            move,
            ping,
            repairwallet,
            resendtx,
            reservebalance,
            sendalert,
            sendfrom,
            sendmany,
            sendrawtransaction,
            sendtoaddress,
            setaccount,
            setgenerate,
            settxfee,
            signmessage,

            //signrawtransaction,
            stop,

            submitblock,
            validateaddress,
            validatepubkey,
            verifymessage,
            walletlock,
            walletpassphrase,
            walletpassphrasechange,
            listBanned,
            clearbanned,
            getchaintips
        }

        public string _serverRpcIp { get; set; }
        public string _serverRpcPort { get; set; }
        public string _rpcUser { get; set; }
        public string _rpcPass { get; set; }

        public string RpcIp
        {
            get { return _serverRpcIp; }
            set { _serverRpcIp = value; }
        }

        public string RpcPort
        {
            get { return _serverRpcPort; }
            set { _serverRpcPort = value; }
        }

        public string RpcUser
        {
            get { return _rpcUser; }
            set { _rpcUser = value; }
        }

        public string RpcPass
        {
            get { return _rpcPass; }
            set { _rpcPass = value; }
        }

        public void SetRpcCredentials(String rpcUser, String rpcPass)
        {
            RpcUser = rpcUser;
            RpcPass = rpcPass;
        }

        public void SetRpcHost(String rpcIp, String rpcPort)
        {
            RpcIp = rpcIp;
            RpcPort = rpcPort;
        }

        public void Init(NodeRPC rpc)
        {
            NodeIndicator = rpc.NodeIndicator;
            RpcIp = rpc.NodeIp;
            RpcPort = rpc.NodePort;
            RpcUser = rpc.NodeUsername;
            RpcPass = rpc.NodePassword;
        }

        public String AddMultiSigAddress(Int32 nRequired, String key1, String key2)
        {
            JArray jar = new JArray();
            jar.Add(key1);
            jar.Add(key2);
            return RequestServer(MethodName.addmultisigaddress, new List<object> {nRequired, jar})["result"]
                .ToString();
        }

        public String AddMultiSigAddress(Int32 nRequired, String key1, String key2, String account)
        {
            JArray jar = new JArray();
            jar.Add(key1);
            jar.Add(key2);
            return RequestServer(MethodName.addmultisigaddress, new List<object> {nRequired, jar, account})
                ["result"].ToString();
        }

        public String AddNode(String node, NodeAction action)
        {
            return RequestServer(MethodName.addnode, new List<object> {node, action.ToString()})["result"]
                .ToString();
        }

        public String AddRedeemScript(String redeemScript)
        {
            return RequestServer(MethodName.addredeemscript, redeemScript)["result"].ToString();
        }

        public String AddRedeemScript(String redeemScript, String account)
        {
            return RequestServer(MethodName.addredeemscript, new List<object> {redeemScript, account})["result"]
                .ToString();
        }

        public String BackupWallet(String destination)
        {
            return RequestServer(MethodName.backupwallet, destination)["result"].ToString();
        }

        public String CheckWallet()
        {
            return RequestServer(MethodName.checkwallet)["result"].ToString();
        }

        public String DecodeRawTransaction(String rawTransaction)
        {
            return RequestServer(MethodName.decoderawtransaction, rawTransaction)["result"].ToString();
        }

        public String DecodeScript(String hexString)
        {
            return RequestServer(MethodName.decodescript, hexString)["result"].ToString();
        }

        public String DumpPrivKey(String polytimosAddress)
        {
            return RequestServer(MethodName.dumpprivkey, polytimosAddress)["result"].ToString();
        }

        public String DumpWallet(String fileName)
        {
            return RequestServer(MethodName.dumpwallet, fileName)["result"].ToString();
        }

        public String EncryptWallet(String passphrase)
        {
            return RequestServer(MethodName.encryptwallet, passphrase)["result"].ToString();
        }

        public String GetAccount(String polytimosAddress)
        {
            return RequestServer(MethodName.getaccount, polytimosAddress)["result"].ToString();
        }

        public String GetAccountAddress(String polytimosAccount)
        {
            return RequestServer(MethodName.getaccountaddress, polytimosAccount)["result"].ToString();
        }

        public String GetAddedNodeInfo(String dns)
        {
            return RequestServer(MethodName.getaddednodeinfo, dns)["result"].ToString();
        }

        public String GetAddedNodeInfo(String dns, String node)
        {
            return RequestServer(MethodName.getaddednodeinfo, new List<object> {dns, node})["result"].ToString();
        }

        public String GetAddressesByAccount(String account)
        {
            return RequestServer(MethodName.getaddressesbyaccount, account)["result"].ToString();
        }

        public Decimal GetBalance()
        {
            String result = RequestServer(MethodName.getbalance)["result"].ToString();
            Decimal balance;
            Decimal.TryParse(result, out balance);
            return balance;
        }

        public Decimal GetBalance(String account)
        {
            String result = RequestServer(MethodName.getbalance, account)["result"].ToString();
            Decimal balance;
            Decimal.TryParse(result, out balance);
            return balance;
        }

        public Decimal GetBalance(String account, Int32 minConf)
        {
            String result = RequestServer(MethodName.getbalance, new List<object> {account, minConf})["result"]
                .ToString();
            Decimal balance;
            Decimal.TryParse(result, out balance);
            return balance;
        }

        public String GetBestBlockHash()
        {
            return RequestServer(MethodName.getbestblockhash)["result"].ToString();
        }

        public async Task<string> GetBlock(string hash)
        {
            return RequestServer(MethodName.getblock, hash)["result"].ToString();
        }

        public Task<string> GetBlock(int height)
        {
            throw new NotImplementedException();
        }

        public Task<string> SearchTransactions(string address)
        {
            throw new NotImplementedException();
        }

        public String GetBlock(String hash, Boolean verbose)
        {
            return RequestServer(MethodName.getblock, new List<object> {hash, verbose})["result"].ToString();
        }

        public String GetBlockByNumber(Int64 index)
        {
            return RequestServer(MethodName.getblockbynumber, index)["result"].ToString();
        }

        public String GetBlockByNumber(Int64 index, Boolean verbos)
        {
            return RequestServer(MethodName.getblockbynumber, new List<object> {index, verbos})["result"]
                .ToString();
        }

        public async Task<int> GetBlockCount()
        {
            String result = RequestServer(MethodName.getblockcount)["result"].ToString();
            Int32 balance;
            balance = Int32.Parse(result);

            return balance;
        }

        public async Task<string> GetBlockHash(int index)
        {
            return RequestServer(MethodName.getblockhash, index)["result"].ToString();
        }

        public String GetBlockTemplate()
        {
            return RequestServer(MethodName.getblocktemplate)["result"].ToString();
        }

        public String GetBlockTemplate(params object[] parameters)
        {
            return RequestServer(MethodName.getblocktemplate, new List<object> {parameters})["result"].ToString();
        }

        public String GetCheckpoint()
        {
            return RequestServer(MethodName.getcheckpoint)["result"].ToString();
        }

        public String GetConnectionCount()
        {
            return RequestServer(MethodName.getconnectioncount)["result"].ToString();
        }

        public String GetDifficulty()
        {
            return RequestServer(MethodName.getdifficulty)["result"].ToString();
        }

        public String GetInfo()
        {
            return RequestServer(MethodName.getinfo)["result"].ToString();
        }

        public String GetMiningInfo()
        {
            return RequestServer(MethodName.getmininginfo)["result"].ToString();
        }

        public String GetNetTotals()
        {
            return RequestServer(MethodName.getnettotals)["result"].ToString();
        }

        public String GetNewPubKey(String account)
        {
            return RequestServer(MethodName.getnewpubkey, account)["result"].ToString();
        }

        public String GetPeerInfo()
        {
            return RequestServer(MethodName.getpeerinfo)["result"].ToString();
        }

        public String GetRawMemPool()
        {
            return RequestServer(MethodName.getrawmempool)["result"].ToString();
        }

        public async Task<string> GetRawTransaction(String txId)
        {
            return RequestServer(MethodName.getrawtransaction, txId)["result"].ToString();
        }

        public async Task<string> GetRawTransaction(String txId, int verbose)
        {
            return RequestServer(MethodName.getrawtransaction, new List<object> {txId, verbose})["result"]
                .ToString();
        }

        public String GetReceivedByAccount(String account)
        {
            return RequestServer(MethodName.getreceivedbyaccount, account)["result"].ToString();
        }

        public String GetReceivedByAccount(String account, Int32 minConf)
        {
            return RequestServer(MethodName.getreceivedbyaccount, new List<object> {account, minConf})["result"]
                .ToString();
        }

        public String GetReceivedByAddress(String polytimosAddress)
        {
            return RequestServer(MethodName.getreceivedbyaddress, polytimosAddress)["result"].ToString();
        }

        public String GetReceivedByAddress(String polytimosAddress, Int32 minConf)
        {
            return RequestServer(MethodName.getreceivedbyaddress, new List<object> {polytimosAddress, minConf})
                ["result"].ToString();
        }

        public String GetStakeSubsidy(String hexString)
        {
            return RequestServer(MethodName.getstakesubsidy, hexString)["result"].ToString();
        }

        public String GetStakingInfo()
        {
            return RequestServer(MethodName.getstakinginfo)["result"].ToString();
        }

        public String GetSubsidy()
        {
            return RequestServer(MethodName.getsubsidy)["result"].ToString();
        }

        public String GetSubsidy(Int32 nTarget)
        {
            return RequestServer(MethodName.getsubsidy, nTarget)["result"].ToString();
        }

        public String GetTransaction(String txid)
        {
            return RequestServer(MethodName.gettransaction, txid)["result"].ToString();
        }

        public String GetWork()
        {
            return RequestServer(MethodName.getwork)["result"].ToString();
        }

        public String GetWork(String data)
        {
            return RequestServer(MethodName.getwork, data)["result"].ToString();
        }

        public String GetWorkEx()
        {
            return RequestServer(MethodName.getworkex)["result"].ToString();
        }

        public String Help()
        {
            return RequestServer(MethodName.help)["result"].ToString();
        }

        public String Help(String command)
        {
            return RequestServer(MethodName.help, command)["result"].ToString();
        }

        public String ImportPrivKey(String polytimosPrivKey)
        {
            return RequestServer(MethodName.importprivkey, polytimosPrivKey)["result"].ToString();
        }

        public String ImportPrivKey(String polytimosPrivKey, String label)
        {
            return RequestServer(MethodName.importprivkey, new List<object> {polytimosPrivKey, label})["result"]
                .ToString();
        }

        public String ImportPrivKey(String polytimosPrivKey, String label, Boolean rescan)
        {
            return RequestServer(MethodName.importprivkey, new List<object> {polytimosPrivKey, label, rescan})
                ["result"].ToString();
        }

        public String ImportWallet(String fileName)
        {
            return RequestServer(MethodName.importwallet, fileName)["result"].ToString();
        }

        public String KeyPoolRefill()
        {
            return RequestServer(MethodName.keypoolrefill)["result"].ToString();
        }

        public String KeyPoolRefill(Int32 newSize)
        {
            return RequestServer(MethodName.keypoolrefill, newSize)["result"].ToString();
        }

        public String ListAccounts()
        {
            return RequestServer(MethodName.listaccounts)["result"].ToString();
        }

        public String ListAccounts(Int32 minConf)
        {
            return RequestServer(MethodName.listaccounts, minConf)["result"].ToString();
        }

        public String ListAddressGroupings()
        {
            return RequestServer(MethodName.listaddressgroupings)["result"].ToString();
        }

        public String ListReceivedByAccount()
        {
            return RequestServer(MethodName.listreceivedbyaccount)["result"].ToString();
        }

        public String ListReceivedByAccount(Int32 minConf)
        {
            return RequestServer(MethodName.listreceivedbyaccount, minConf)["result"].ToString();
        }

        public String ListReceivedByAccount(Int32 minConf, Boolean includeEmpty)
        {
            return RequestServer(MethodName.listreceivedbyaccount, new List<object> {minConf, includeEmpty})
                ["result"].ToString();
        }

        public String ListReceivedByAddress()
        {
            return RequestServer(MethodName.listreceivedbyaddress)["result"].ToString();
        }

        public String ListReceivedByAddress(Int32 minConf)
        {
            return RequestServer(MethodName.listreceivedbyaddress, minConf)["result"].ToString();
        }

        public String ListReceivedByAddress(Int32 minConf, Boolean includeEmpty)
        {
            return RequestServer(MethodName.listreceivedbyaddress, new List<object> {minConf, includeEmpty})
                ["result"].ToString();
        }

        public String ListSinceBlock()
        {
            return RequestServer(MethodName.listsinceblock)["result"].ToString();
        }

        public String ListSinceBlock(String blockHash)
        {
            return RequestServer(MethodName.listsinceblock, blockHash)["result"].ToString();
        }

        public String ListSinceBlock(String blockHash, Int32 targetConf)
        {
            return RequestServer(MethodName.listsinceblock, new List<object> {blockHash, targetConf})["result"]
                .ToString();
        }

        public String ListTransactions()
        {
            return RequestServer(MethodName.listtransactions)["result"].ToString();
        }

        public String ListTransactions(String account)
        {
            return RequestServer(MethodName.listtransactions, account)["result"].ToString();
        }

        public String ListTransactions(String account, Int32 count)
        {
            return RequestServer(MethodName.listtransactions, new List<object> {account, count})["result"]
                .ToString();
        }

        public String ListTransactions(String account, Int32 count, Int64 from)
        {
            return RequestServer(MethodName.listtransactions, new List<object> {account, count, from})["result"]
                .ToString();
        }

        public String ListUnspent()
        {
            return RequestServer(MethodName.listunspent)["result"].ToString();
        }

        public String ListUnspent(Int32 minConf)
        {
            return RequestServer(MethodName.listunspent, minConf)["result"].ToString();
        }

        public String ListUnspent(Int32 minConf, Int32 maxConf)
        {
            return RequestServer(MethodName.listunspent, new List<object> {minConf, maxConf})["result"].ToString();
        }

        public String MakeKeyPair()
        {
            return RequestServer(MethodName.makekeypair)["result"].ToString();
        }

        public String MakeKeyPair(String prefix)
        {
            return RequestServer(MethodName.makekeypair, prefix)["result"].ToString();
        }

        public String Move(String fromPolytimosAccount, String toPolytimosAccount, Decimal amount)
        {
            return RequestServer(MethodName.move,
                new List<object> {fromPolytimosAccount, toPolytimosAccount, amount})["result"].ToString();
        }

        public String Move(String fromPolytimosAccount, String toPolytimosAccount, Decimal amount, Int32 minConf)
        {
            return RequestServer(MethodName.move,
                new List<object> {fromPolytimosAccount, toPolytimosAccount, amount, minConf})["result"].ToString();
        }

        public String Move(String fromPolytimosAccount, String toPolytimosAccount, Decimal amount, Int32 minConf,
            String comment)
        {
            return RequestServer(MethodName.move,
                    new List<object> {fromPolytimosAccount, toPolytimosAccount, amount, minConf, comment})["result"]
                .ToString();
        }

        public String Ping()
        {
            return RequestServer(MethodName.ping)["result"].ToString();
        }

        public String RepairWallet()
        {
            return RequestServer(MethodName.repairwallet)["result"].ToString();
        }

        public String ResendTx()
        {
            return RequestServer(MethodName.resendtx)["result"].ToString();
        }

        public String GetChainTips()
        {
            return RequestServer(MethodName.getchaintips)["result"].ToString();
        }

        public String ReserveBalance(Boolean reserve, Decimal ammount)
        {
            return RequestServer(MethodName.reservebalance, new List<object> {reserve, ammount})["result"]
                .ToString();
        }

        public String SendAlert(String message, String privateKey, String minVersion, String maxVersion,
            int priority, String id)
        {
            return RequestServer(MethodName.sendalert,
                new List<object> {message, privateKey, minVersion, maxVersion, priority, id})["result"].ToString();
        }

        public String SendAlert(String message, String privateKey, String minVersion, String maxVersion,
            int priority, String id, String cancelUpTo)
        {
            return RequestServer(MethodName.sendalert,
                    new List<object>
                        {message, privateKey, minVersion, maxVersion, priority, id, cancelUpTo})["result"]
                .ToString();
        }

        public String SendFrom(String fromAccount, String toPolytimosAddress, Decimal amount)
        {
            return RequestServer(MethodName.sendfrom, new List<object> {fromAccount, toPolytimosAddress, amount})
                ["result"].ToString();
        }

        public String SendFrom(String fromAccount, String toPolytimosAddress, Decimal amount, Int32 minConf)
        {
            return RequestServer(MethodName.sendfrom,
                new List<object> {fromAccount, toPolytimosAddress, amount, minConf})["result"].ToString();
        }

        public String SendFrom(String fromAccount, String toPolytimosAddress, Decimal amount, Int32 minConf,
            String comment)
        {
            return RequestServer(MethodName.sendfrom,
                new List<object> {fromAccount, toPolytimosAddress, amount, minConf, comment})["result"].ToString();
        }

        public String SendFrom(String fromAccount, String toPolytimosAddress, Decimal amount, Int32 minConf,
            String comment, String commentTo)
        {
            return RequestServer(MethodName.sendfrom,
                    new List<object>
                        {fromAccount, toPolytimosAddress, amount, minConf, comment, commentTo})["result"]
                .ToString();
        }

        public String SendMany(String fromAccount, List<string> polytimosAddress, List<Decimal> ammount)
        {
            JObject Joe = new JObject();

            if (ammount.Count == 1)
            {
                for (int i = 0; i < polytimosAddress.Count; i++)
                {
                    Joe[polytimosAddress[i].ToString()] = ammount[0];
                }
            }
            else
            {
                if (ammount.Count == polytimosAddress.Count)
                {
                    for (int i = 0; i < polytimosAddress.Count; i++)
                    {
                        Joe[polytimosAddress[i].ToString()] = ammount[i];
                    }
                }
                else
                {
                    throw new Exception(
                        "The number of elements in the Ammount list is neither equal to 1 or equal to the number of Polytimos addresses in the Polytimos Address list!");
                }
            }

            return RequestServer(MethodName.sendmany, new List<object> {fromAccount, Joe})["result"].ToString();
        }

        public String SendMany(String fromAccount, List<string> polytimosAddress, List<Decimal> ammount,
            Int32 minConf)
        {
            JObject Joe = new JObject();

            if (ammount.Count == 1)
            {
                for (int i = 0; i < polytimosAddress.Count; i++)
                {
                    Joe[polytimosAddress[i].ToString()] = ammount[0];
                }
            }
            else
            {
                if (ammount.Count == polytimosAddress.Count)
                {
                    for (int i = 0; i < polytimosAddress.Count; i++)
                    {
                        Joe[polytimosAddress[i].ToString()] = ammount[i];
                    }
                }
                else
                {
                    throw new Exception(
                        "The number of elements in the Ammount list is neither equal to 1 or equal to the number of Polytimos addresses in the Polytimos Address list!");
                }
            }

            return RequestServer(MethodName.sendmany, new List<object> {fromAccount, Joe, minConf})["result"]
                .ToString();
        }

        public String SendMany(String fromAccount, List<string> polytimosAddress, List<Decimal> ammount,
            Int32 minConf, String comment)
        {
            JObject Joe = new JObject();

            if (ammount.Count == 1)
            {
                for (int i = 0; i < polytimosAddress.Count; i++)
                {
                    Joe[polytimosAddress[i].ToString()] = ammount[0];
                }
            }
            else
            {
                if (ammount.Count == polytimosAddress.Count)
                {
                    for (int i = 0; i < polytimosAddress.Count; i++)
                    {
                        Joe[polytimosAddress[i].ToString()] = ammount[i];
                    }
                }
                else
                {
                    throw new Exception(
                        "The number of elements in the Ammount list is neither equal to 1 or equal to the number of Polytimos addresses in the Polytimos Address list!");
                }
            }

            return RequestServer(MethodName.sendmany, new List<object> {fromAccount, Joe, minConf, comment})
                ["result"].ToString();
        }

        public String SendRawTransaction(String signedRawTransaction)
        {
            return RequestServer(MethodName.sendrawtransaction, signedRawTransaction)["result"].ToString();
        }

        public String SendToAddress(String polytimosAddress, Decimal amount)
        {
            return RequestServer(MethodName.sendtoaddress, new List<object> {polytimosAddress, amount})["result"]
                .ToString();
        }

        public String SendToAddress(String polytimosAddress, Decimal amount, String comment)
        {
            return RequestServer(MethodName.sendtoaddress, new List<object> {polytimosAddress, amount, comment})
                ["result"].ToString();
        }

        public String SendToAddress(String polytimosAddress, Decimal amount, String comment, String commentTo)
        {
            return RequestServer(MethodName.sendtoaddress,
                new List<object> {polytimosAddress, amount, comment, commentTo})["result"].ToString();
        }

        public String SetAccount(String polytimosAddress, String account)
        {
            return RequestServer(MethodName.setaccount, new List<object> {polytimosAddress, account})["result"]
                .ToString();
        }

        public String SetGenerate(Boolean generate)
        {
            return RequestServer(MethodName.setgenerate, generate)["result"].ToString();
        }

        public String SetGenerate(Boolean generate, Int32 genProcLimit)
        {
            return RequestServer(MethodName.setgenerate, new List<object> {generate, genProcLimit})["result"]
                .ToString();
        }

        public bool SetTxFee(Decimal amount)
        {
            return Boolean.Parse(RequestServer(MethodName.settxfee, amount)["result"].ToString());
        }

        public String SignMessage(String polytimosAddress, String message)
        {
            return RequestServer(MethodName.signmessage, new List<object> {polytimosAddress, message})["result"]
                .ToString();
        }

        public String Stop()
        {
            return RequestServer(MethodName.stop)["result"].ToString();
        }

        public String SubmitBlock(String hexData)
        {
            return RequestServer(MethodName.submitblock, hexData)["result"].ToString();
        }

        public Task<string> ValidateAddress(string polytimosAddress)
        {
            return Task.FromResult(RequestServer(MethodName.validateaddress, polytimosAddress)["result"]
                .ToString());
        }

        public String ValidatePubKey(String polytimosPubKey)
        {
            return RequestServer(MethodName.validatepubkey, polytimosPubKey)["result"].ToString();
        }

        public Task<List<QortalPayment>> GetPaymentsByBlock(string blockSignature)
        {
            throw new NotImplementedException();
        }

        public String VerifyMessage(String polytimosAddress, String signature, String message)
        {
            return RequestServer(MethodName.verifymessage, new List<object> {polytimosAddress, signature, message})
                ["result"].ToString();
        }

        public String WalletLock()
        {
            return RequestServer(MethodName.walletlock)["result"].ToString();
        }

        public String WalletPassphrase(String passphrase, Object timeout)
        {
            return RequestServer(MethodName.walletpassphrase, new List<object> {passphrase, timeout})["result"]
                .ToString();
        }

        public String WalletPassphraseChange(String oldPassphrase, String newPassphrase)
        {
            return RequestServer(MethodName.walletpassphrasechange, new List<object> {oldPassphrase, newPassphrase})
                ["result"].ToString();
        }

        public JArray ListBanned()
        {
            return JArray.Parse(RequestServer(MethodName.listBanned)["result"].ToString());
        }

        public void ClearBanned()
        {
            RequestServer(MethodName.clearbanned);
        }



        /// <summary>
        /// ServerRequest handling
        /// </summary>
        /// <param name="methodName"></param>
        /// <returns></returns>
        private JObject RequestServer(MethodName methodName)
        {
            return RequestServer(methodName, new List<object> { });
        }

        private JObject RequestServer(MethodName methodName, object parameter)
        {
            return RequestServer(methodName, new List<object> {parameter});
        }

        private JObject RequestServer(MethodName methodName, object parameter, object parameter2)
        {
            return RequestServer(methodName, new List<object> {parameter, parameter2});
        }

        private JObject RequestServer(MethodName methodName, object parameter, object parameter2, object parameter3)
        {
            return RequestServer(methodName, new List<object> {parameter, parameter2, parameter3});
        }

        private JObject RequestServer(MethodName methodName, List<object> parameters)
        {
            HttpWebRequest rawRequest = GetRawRequest();

            // basic info required by qt
            JObject jObject = new JObject
            {
                new JProperty("jsonrpc", "1.0"),
                new JProperty("id", "1"),
                new JProperty("method", methodName.ToString())
            };

            // adds provided parameters
            JArray props = new JArray();

            if (parameters != null && parameters.Any())
            {
                foreach (object parameter in parameters)
                {
                    props.Add(parameter);
                }
            }

            StreamReader streamReader = null;
            jObject.Add(new JProperty("params", props));

            // serialize json for the request
            try
            {
                String s = JsonConvert.SerializeObject(jObject);
                byte[] byteArray = Encoding.UTF8.GetBytes(s);
                rawRequest.ContentLength = byteArray.Length;
                Stream dataStream = rawRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse webResponse = rawRequest.GetResponse();
                streamReader = new StreamReader(webResponse.GetResponseStream(), true);
                return (JObject) JsonConvert.DeserializeObject(streamReader.ReadToEnd());
            }
            catch (WebException webException)
            {
                if (webException.Status == WebExceptionStatus.ConnectFailure)
                {
                    throw new Exception(
                        "Could not connect to Polytimos host, please check that Polytimos is up and running and that your configuration (" +
                        "http://" + RpcIp + ":" + RpcPort + ") is correct");
                }

                return null;
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                }
            }
        }

        private HttpWebRequest GetRawRequest()
        {
            HttpWebRequest webRequest =
                (HttpWebRequest) WebRequest.Create("http://" + RpcIp + ":" + RpcPort);
            //Console.WriteLine("http://" + _serverRpcIp + ":" + _serverRpcPort);
            //Console.WriteLine(_rpcUser + " " + _rpcPass);

            webRequest.Credentials = new NetworkCredential(RpcUser, RpcPass);
            webRequest.ContentType = "application/json-rpc";
            webRequest.Method = "POST";
            return webRequest;
        }
    }
}