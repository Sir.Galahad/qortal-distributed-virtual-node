﻿using System;

namespace QortalDistributedFederatedNode.WalletHandles
{

    public abstract class WalletHandleBase
    {
        public string _serverRpcIp { get; set; }
        public string _serverRpcPort { get; set; }
        public string _rpcUser { get; set; }
        public string _rpcPass { get; set; }

        public string RpcIp
        {
            get { return _serverRpcIp; }
            set { _serverRpcIp = value; }
        }

        public string RpcPort
        {
            get { return _serverRpcPort; }
            set { _serverRpcPort = value; }
        }

        public string RpcUser
        {
            get { return _rpcUser; }
            set { _rpcUser = value; }
        }

        public string RpcPass
        {
            get { return _rpcPass; }
            set { _rpcPass = value; }
        }

        public void SetRpcCredentials(String rpcUser, String rpcPass)
        {
            RpcUser = rpcUser;
            RpcPass = rpcPass;
        }

        public void SetRpcHost(String rpcIp, String rpcPort)
        {
            RpcIp = rpcIp;
            RpcPort = rpcPort;
        }
    }
}

