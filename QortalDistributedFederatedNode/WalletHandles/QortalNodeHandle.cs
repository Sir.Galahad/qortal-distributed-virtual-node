﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using QortalDistributedFederatedNode.Models;

namespace QortalDistributedFederatedNode.WalletHandles
{
    public class QortalNodeHandle
    {
        public string NodeIndicator { get; set; }

        public string _serverRpcIp { get; set; }
        public string _serverRpcPort { get; set; }
        public string _rpcUser { get; set; }
        public string _rpcPass { get; set; }

        public string RpcIp
        {
            get { return _serverRpcIp; }
            set { _serverRpcIp = value; }
        }

        public string RpcPort
        {
            get { return _serverRpcPort; }
            set { _serverRpcPort = value; }
        }

        public string RpcUser
        {
            get { return _rpcUser; }
            set { _rpcUser = value; }
        }

        public string RpcPass
        {
            get { return _rpcPass; }
            set { _rpcPass = value; }
        }

        public void SetRpcCredentials(String rpcUser, String rpcPass)
        {
            RpcUser = rpcUser;
            RpcPass = rpcPass;
        }

        public void SetRpcHost(String rpcIp, String rpcPort)
        {
            RpcIp = rpcIp;
            RpcPort = rpcPort;
        }

        public void Init(NodeRPC rpc)
        {
            NodeIndicator = rpc.NodeIndicator;
            RpcIp = rpc.NodeIp;
            RpcPort = rpc.NodePort;
            RpcUser = rpc.NodeUsername;
            RpcPass = rpc.NodePassword;
        }

        public async Task<bool> SendChat(byte[] data, int groupId, AddressKeyPair addr, int tries = -1)
        {
            string reference = await GetNextReference(addr.Address);

            byte[] refBytes = SimpleBase.Base58.Bitcoin.Decode(reference).ToArray();
            
            byte[] firstFour = refBytes.ToArray().ToList().GetRange(0, 4).ToArray();

            for (int i = 0; i < 4; i++)
            {
                data[i] = firstFour[i]; //copy reference to message. theres a much better way of doing this soemwhere.
            }
            
            
            QortalUnsignedChatMessageInfo inf = new QortalUnsignedChatMessageInfo()
            {
                Timestamp = DateTimeOffset.Now.ToUnixTimeMilliseconds(),
                Reference = reference,
                Fee = 0,
                TxGroupId = groupId,
                SenderPublicKey = addr.PublicKey,
                Data = SimpleBase.Base58.Bitcoin.Encode(data),
                IsText = true,
                IsEncrypted = false
            };

            bool isSent = false;
            
            while (!isSent && (tries < 0 || tries > 0)) // this is horrible... also needs cancelToken
            {
                try
                {
                    string b58ChatTransaction = await BuildRawChatTransaction(inf);
                    b58ChatTransaction =
                        await ComputerRawChatTransactionNonce(b58ChatTransaction);

                    QortalTransactionSignInfo signInf = new QortalTransactionSignInfo()
                    {
                        PrivateKey = addr.PrivateKey,
                        TransactionBytes = b58ChatTransaction
                    };

                    string signedTransaction = await SignTransaction(signInf);

                    isSent = bool.Parse(await ProcessTransaction(signedTransaction));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                if (!isSent) await Task.Delay(TimeSpan.FromMilliseconds(250));

                tries--;
            }

            return isSent;
        }

        public async Task<string> BuildRawChatTransaction(QortalUnsignedChatMessageInfo info)
        {
            String node = $"http://{RpcIp}:{RpcPort}/chat";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node, JsonConvert.SerializeObject(info), isJson: true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }
        
        public async Task<string> ComputerRawChatTransactionNonce(string b58Transaction)
        {
            String node = $"http://{RpcIp}:{RpcPort}/chat/compute";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node, b58Transaction);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }
        
        public async Task<string> GetBlock(string hash)
        {
            String BlockDataURL = $"http://{RpcIp}:{RpcPort}/blocks/signature/{hash}";

            string jsonBlock = null;

            try
            {
                jsonBlock = await RequestServer(BlockDataURL);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return jsonBlock;
        }

        public async Task<int> GetBlockCount()
        {
            String HeightURL = $"http://{RpcIp}:{RpcPort}/blocks/height";

            string heightString = null;

            heightString = await RequestServer(HeightURL);

            return int.Parse(heightString);
        }

        public async Task<string> GetBlock(int height)
        {
            String HeightURL = $"http://{RpcIp}:{RpcPort}/blocks/byheight/{height}";

            string Hash = null;

            try
            {
                Hash = await RequestServer(HeightURL);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return Hash;
        }

        public async Task<QortalAddressInfo> GetAddressInfo(string address)
        {
            String URL = $"http://{RpcIp}:{RpcPort}/addresses/{address}";

            QortalAddressInfo data = null;

            try
            {
                data = JsonConvert.DeserializeObject<QortalAddressInfo>(await RequestServer(URL));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return data;
        }

        public async Task<string> GetBlockByHeight(int index)
        {
            String HeightURL = $"http://{RpcIp}:{RpcPort}/blocks/byheight/{index}";

            string Hash = null;

            try
            {
                Hash = await RequestServer(HeightURL);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return Hash;
        }

        public async Task<Decimal> GetAddressBalance(string address)
        {
            String HeightURL = $"http://localhost:12391/addresses/balance/{address}";

            decimal Hash;



            try
            {
                string str = await RequestServer(HeightURL);

                Hash = Decimal.Parse(str, NumberStyles.AllowExponent | NumberStyles.AllowDecimalPoint,
                    CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return Hash;
        }

        public async Task<Decimal> GetAddressWorth(string address)
        {
            Decimal AddyWorth = 0;


            List<QortalPayment> addyPayments =
                JsonConvert.DeserializeObject<List<QortalPayment>>(await SearchTransactions(address));


            foreach (QortalPayment adPayment in addyPayments)
            {
                if (adPayment.CreatorAddress == address)
                {
                    AddyWorth -= adPayment.Amount + adPayment.Fee;
                }

                if (adPayment.Recipient == address)
                {
                    AddyWorth += adPayment.Amount;
                }
            }

            return AddyWorth;
        }
        
        public async Task<string> SearchTransactions(string address)
        {
            String transactionUrl =
                $"http://{RpcIp}:{RpcPort}/transactions/search?txType=PAYMENT&address={address}&confirmationStatus=BOTH&limit=0&offset=0";

            string jsonBlock = null;

            try
            {
                jsonBlock = await RequestServer(transactionUrl);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return jsonBlock;
        }

        public async Task<string> SignTransaction(QortalTransactionSignInfo info)
        {
            String node = $"http://{RpcIp}:{RpcPort}/transactions/sign";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node, JsonConvert.SerializeObject(info), isJson: true);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }

        public async Task<List<QortalPayment>> GetPaymentsByBlock(string signature)
        {
            string paymentsURL = $"http://{RpcIp}:{RpcPort}/transactions/block/{signature}";

            List<QortalPayment> payments = new List<QortalPayment>();

            try
            {
                payments = JsonConvert.DeserializeObject<List<QortalPayment>>(await RequestServer(paymentsURL));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return payments;
        }

        public async Task<string> GetNextReference(string address)
        {
            String node = $"http://{RpcIp}:{RpcPort}/addresses/lastreference/{address}";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }

        public async Task<List<QortalChatMessage>> SearchMessages(int txGroupId, DateTimeOffset? after = null)
        {
            String chatUrl = null;

            if (after == null)
            {
                chatUrl = $"http://{RpcIp}:{RpcPort}/chat/messages?txGroupId={txGroupId}&limit=0";
            }
            else
            {
                chatUrl = $"http://{RpcIp}:{RpcPort}/chat/messages?after={after?.ToUnixTimeMilliseconds()}&txGroupId={txGroupId}&limit=0";
            }

            List<QortalChatMessage> jsonBlock = null;

            try
            {
                string datablock = await RequestServer(chatUrl);
                
                jsonBlock = JsonConvert.DeserializeObject<List<QortalChatMessage>>(datablock);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return jsonBlock;
        }


        public async Task<string> ProcessTransaction(string rawTrans)
        {
            String node = $"http://{RpcIp}:{RpcPort}/transactions/process";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node, rawTrans);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }

        public async Task<string> ValidateAddress(string address)
        {
            String node = $"http://{RpcIp}:{RpcPort}/addresses/validate/{address}";

            String NodeData = null;

            try
            {
                NodeData = await RequestServer(node);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return NodeData;
        }

        public async Task<string> GetEntropyFromWallet(int nBytes)
        {
            String randomDataUrl = $"http://{RpcIp}:{RpcPort}/utils/random?length=16";

            String randomData = null;

            try
            {
                randomData = await RequestServer(randomDataUrl);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return randomData;
        }

        public async Task<string> ConvertEntropyToPrivatekey(string entropy)
        {
            String privateKeyUrl = $"http://{RpcIp}:{RpcPort}/utils/privatekey";

            string privateKey = null;

            try
            {
                privateKey = await RequestServer(privateKeyUrl, entropy);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return privateKey;
        }

        public async Task<string> ConvertPrivateKeyToPublicKey(string privateKey)
        {
            String publicKeyUrl = $"http://{RpcIp}:{RpcPort}/utils/publickey";

            string publicKey = null;

            try
            {
                publicKey = await RequestServer(publicKeyUrl, privateKey);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return publicKey;
        }

        public async Task<string> ConvertPublicKeyToB58Address(string publicKey)
        {
            string addressUrl = $"http://{RpcIp}:{RpcPort}/addresses/convert/{publicKey}";

            string Address = null;

            try
            {
                Address = await RequestServer(addressUrl);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            return Address;
        }

        
        
        

        private HttpClient webClient = new HttpClient();

        private async Task<string> RequestServer(String url, String data = null, bool isJson = false,
            bool isDelete = false)
        {
            try
            {
                if (data != null)
                {
                    try
                    {
                        if (isJson)
                        {
                            HttpWebRequest req = HttpWebRequest.Create(url) as HttpWebRequest;

                            req.ContentType = "application/json";
                            req.Method = "POST";


                            using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                            {
                                streamWriter.Write(data);
                            }

                            var httpResponse = (HttpWebResponse) req.GetResponse();

                            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                            {
                                return await streamReader.ReadToEndAsync();
                            }
                        }
                        else
                        {

                            if (isDelete)
                            {

                                var content = new StringContent(data);

                                var request = new HttpRequestMessage(HttpMethod.Delete, new Uri(url));

                                request.Content = content;

                                HttpResponseMessage result = await webClient.SendAsync(request);

                                return await result.Content.ReadAsStringAsync();

                            }
                            else
                            {
                                var content = new StringContent(data);
                                HttpResponseMessage result = await webClient.PostAsync(url, content);

                                return await result.Content.ReadAsStringAsync();
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
                else
                {
                    string result = await webClient.GetAsync(url).Result.Content.ReadAsStringAsync();

                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }
    }
}