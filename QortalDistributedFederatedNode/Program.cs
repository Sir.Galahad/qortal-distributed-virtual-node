﻿﻿using System;
using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Net;
 using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
 using Newtonsoft.Json;
 using Newtonsoft.Json.Linq;
 using QortalDistributedFederatedNode.Models;
using QortalDistributedFederatedNode.Models.Enums;
 using QortalDistributedFederatedNode.Services;
 using QortalDistributedFederatedNode.WalletHandles;

 namespace QortalDistributedFederatedNode
{
    class Program
    {

        public static BtcNodeHandle infoNode = null;

        public static QortalNodeHandle qNodeHandle = null;

        public static QChatHandlingService ChatHandlingService = null;
            
        public static Task ChatCheckTask = null;

        public static Config Config = null;

        public static AddressKeyPair ChatKeys = null;

        static void Main(string[] args)
        {

            string path = Path.Combine(Environment.CurrentDirectory, "config.json");

            if (!File.Exists(path))
            {
                Models.Config conf = new Config();

                File.WriteAllText(path, JObject.Parse(JsonConvert.SerializeObject(conf)).ToString(Formatting.Indented));

                Console.WriteLine("Config file not found. Config template created.");

                Environment.Exit(0);
            }

            Config = JsonConvert.DeserializeObject<Config>(
                File.ReadAllText(Path.Combine(Environment.CurrentDirectory, "config.json")));


            BtcNodeHandle ha = new BtcNodeHandle();

            ha.Init(Config.InfoNode);

            infoNode = ha;

            qNodeHandle ??= new QortalNodeHandle();

            qNodeHandle.Init(Config.QortalNode);

            new Program().StartAsync().GetAwaiter().GetResult();
        }
        
        public async Task StartAsync()
        {
            ChatKeys ??= GetAddresskpFromPrivKey(Config.QortalChatPrivateKey);           
            
            ChatHandlingService ??= new QChatHandlingService(qNodeHandle, infoNode, ChatKeys);
            
            ChatCheckTask ??= ChatHandlingService.CheckTask();


            /*
            string txHash = "f4184fc596403b9d638783cf57adfe4c75c605f6356fbc91338530e9831e9e16";
            
            QbyteRequestBuilder b = new QbyteRequestBuilder();

            b.ForCoin(QByteLang.Coin.LITECOIN);
            b.WithRequest(QByteLang.RequestType.GETTRANSACTION);
            b.WithDataModifier(QByteLang.DataModifier.BYHASH);
            b.WithData(SimpleBase.Base16.Decode(txHash).ToArray());

            QbyteRequest r = b.Build();

            QbyteReturn ret = await ChatHandlingService.RequestAndWaitAsync(r, TimeSpan.FromMinutes(10));

            */
            await Task.Delay(Timeout.Infinite);
            /* some sample code to help understand how this process might work...       
            try
            {
                
                //TransactionCheckTask ??= TransactionCheckService.CheckTask();

                QbyteRequestBuilder b = new QbyteRequestBuilder();

                b.ForCoin(QByteLang.Coin.LITECOIN);
                b.WithRequest(QByteLang.RequestType.GETTRANSACTION);
                b.WithDataModifier(QByteLang.DataModifier.BYHASH);
                b.WithData(SimpleBase.Base16
                    .Decode("7870286ad46adc27b432724465cb23464c568f5ea6b43df6c6b0ed4ad986fdb8")
                    .ToArray());


                List<byte> EncodedMessage = b.Encode().ToList();

                string hex = SimpleBase.Base16.LowerCase.Encode(EncodedMessage.ToArray());

                Console.WriteLine(hex);

                QbyteReturnBuilder builder = new QbyteReturnBuilder();

                byte[] data = SimpleBase.Base16
                    .Decode(
                        "6969")
                    .ToArray();

                builder.WithData(data);

                Console.WriteLine(SimpleBase.Base16.LowerCase.Encode(data));

                QbyteReturn builtSend = builder.Build(EncodedMessage.Take(4).ToArray());

                List<byte[]> chatMessage = builtSend.Encode();

                List<QbyteReturnPart> parts = new List<QbyteReturnPart>();

                foreach (byte[] msg in chatMessage)
                {
                    parts.Add(new QbyteReturnPart().Decode(msg.ToList()));
                }

                foreach (byte[] msg in chatMessage)
                {
                    if (QChatInfoBuilderService.TryAddPart(msg.ToList(), out QbyteReturnPart part))
                    {
                        Console.WriteLine($"part {(uint) part.PartNum}/{(uint) part.PartCount} accepted");
                    }
                    else
                    {
                        Console.WriteLine($"part {(uint) part.PartNum}/{(uint) part.PartCount} denied");
                    }
                }

                List<QbyteReturn> builtReturns = QChatInfoBuilderService.Assemble();

                Console.WriteLine(SimpleBase.Base16.LowerCase.Encode(builtReturns.First()._data));

                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            Console.ReadKey();

            */



        }
        
        static AddressKeyPair GetAddresskpFromPrivKey(string privateKey)
        {
            

            return new AddressKeyPair()
            {
                Address = qNodeHandle.ConvertPublicKeyToB58Address(qNodeHandle.ConvertPrivateKeyToPublicKey(privateKey).Result).Result,
                PrivateKey = privateKey,
                PublicKey = qNodeHandle.ConvertPrivateKeyToPublicKey(privateKey).Result
            };
        }

    }
}