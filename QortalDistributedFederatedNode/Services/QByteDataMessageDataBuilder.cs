﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using QortalDistributedFederatedNode.Models;
using QortalDistributedFederatedNode.Models.Enums;

namespace QortalDistributedFederatedNode.Services
{
    public class QByteDataMessageDataBuilder
    {

        private List<QbyteReturnPart> _partPool = new List<QbyteReturnPart>();

        private List<QbyteReturn> _returnPool = new List<QbyteReturn>();

        private List<QbyteRequest> _requestPool = new List<QbyteRequest>();
        
        public IReadOnlyCollection<QbyteReturnPart> PartPool => _partPool.AsReadOnly();

        public IReadOnlyCollection<QbyteReturn> ReturnPool => _returnPool.AsReadOnly();

        public IReadOnlyCollection<QbyteRequest> RequestPool => _requestPool.AsReadOnly();

        public static QByteLang.ProcessType GetType(QortalChatMessage message)
        {
            List<byte> encoded = SimpleBase.Base58.Bitcoin.Decode(message.Data).ToArray().ToList();


            byte procType = encoded[5];

            return (QByteLang.ProcessType) procType;
        }


        /// <summary>
        /// returns true if part was added
        /// returns false with part if part already exists
        /// returns false without part if part fails to decode
        /// </summary>
        /// <param name="encoded"></param>
        /// <param name="part"></param>
        /// <returns></returns>
        public bool TryAddPart(List<byte> encoded, out QbyteReturnPart part)
        {
            QbyteReturnPart qbp = null;

            try
            {
                qbp = new QbyteReturnPart();

                qbp.Decode(encoded);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                part = null;

                return false;
            }

            part = qbp;

            if (!_partPool.Any(x => x.CompleteDataHash == qbp.CompleteDataHash && x.PartNum == qbp.PartNum))
            {
                _partPool.Add(part);

                List<QbyteReturn> newAssembledReturns = Assemble();

                foreach (QbyteReturn ret in newAssembledReturns)
                {
                    if (_returnPool.All(x => x.JobHash != ret.JobHash))
                    {
                        _returnPool.Add(ret);
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        public List<QbyteReturn> Assemble()
        {

            List<IGrouping<byte[], QbyteReturnPart>> partGroups = _partPool.GroupBy(x => x.CompleteDataHash).ToList();

            List<IGrouping<byte[], QbyteReturnPart>>
                completePartGroups = new List<IGrouping<byte[], QbyteReturnPart>>();

            foreach (IGrouping<byte[], QbyteReturnPart> partGroup in partGroups)
            {

                bool completeFlag = true;

                for (int i = 0; i < partGroup.Count(); i++)
                {
                    completeFlag = partGroup.Any(x => x.PartNum == i);

                    if (!completeFlag) break;
                }

                if (completeFlag)
                {
                    completePartGroups.Add(partGroup);
                }
            }

            List<IGrouping<byte[], QbyteReturnPart>> unconstructedGroups =
                new List<IGrouping<byte[], QbyteReturnPart>>();

            foreach (IGrouping<byte[], QbyteReturnPart> group in completePartGroups)
            {
                if (!_returnPool.Any(x => x.JobHash == group.Key))
                {
                    unconstructedGroups.Add(group);
                }
            }

            List<QbyteReturn> constructedReturns = new List<QbyteReturn>();

            foreach (IGrouping<byte[], QbyteReturnPart> partGroup in unconstructedGroups)
            {

                List<byte> dataBlock = new List<byte>();

                for (int i = 0; i < partGroup.Count(); i++)
                {
                    dataBlock.AddRange(partGroup.ToArray()[i].Data);
                }

                QbyteReturn qbr = new QbyteReturn(partGroup.Key, 0x1, dataBlock.ToArray());

                constructedReturns.Add(qbr);
            }

            return constructedReturns;
        }
    }
}