﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QortalDistributedFederatedNode.Models;
using QortalDistributedFederatedNode.Models.Enums;
using QortalDistributedFederatedNode.WalletHandles;

namespace QortalDistributedFederatedNode.Services
{
    public class QChatHandlingService
    {

        public int txMessageGroupId = 52;

        private List<QortalChatMessage> trackedMessages = new List<QortalChatMessage>();

        private QortalNodeHandle _qNodeHandle;

        private BtcNodeHandle _infoHandle;

        private AddressKeyPair _chatKeypair;

        private DateTimeOffset LastmessageTime;

        private QByteDataMessageDataBuilder qCBuilder;
        
        public QChatHandlingService(QortalNodeHandle qNodeHandle, BtcNodeHandle infoHandle, AddressKeyPair chatKeypair)
        {
            _qNodeHandle = qNodeHandle;
            _infoHandle = infoHandle;
            _chatKeypair = chatKeypair;
            LastmessageTime = DateTimeOffset.Now;
            
            qCBuilder = new QByteDataMessageDataBuilder();
        }


        public async Task<QbyteReturn> RequestAndWaitAsync(QbyteRequest request, TimeSpan timeout)
        {
            
            byte[] msgData = request.Encode();

            bool msgSent = await _qNodeHandle.SendChat(msgData.ToArray(), txMessageGroupId, _chatKeypair);

            if (!msgSent)
            {
                throw new Exception("request not sent");
            }

            Task<QbyteReturn> waitForReturnTask = WaitForFullReturnAsync(request);

            var timeoutTask = Task.Delay(timeout);

            var task = await Task.WhenAny(waitForReturnTask, timeoutTask);
            
            if(task == timeoutTask)
            {
                return null;
            }
            else if(task.IsFaulted)
            {
                return null;
            }
            else if (task.IsCompletedSuccessfully)
            {
                QbyteReturn ret = waitForReturnTask.Result;

                return ret;
            }

            return null;
        }

        private async Task<QbyteReturn> WaitForFullReturnAsync(QbyteRequest request)
        {
            QbyteReturn fullReturn = null;

            while (fullReturn == null)
            {
                fullReturn = qCBuilder.ReturnPool.FirstOrDefault(x => x.JobHash == request.JobHash);
                
                if(fullReturn == null) await Task.Delay(TimeSpan.FromSeconds(5));
            }

            return fullReturn;
        }
        
        public async Task CheckTask()
        {
            while (true)
            {
                try
                {
                    await RunCheck();
                    Console.WriteLine(DateTimeOffset.Now);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);

                }


                await Task.Delay(TimeSpan.FromSeconds(15));
            }
        }


        private async Task RunCheck()
        {
            List<QortalChatMessage> messages = await _qNodeHandle.SearchMessages(txMessageGroupId, LastmessageTime);

            foreach (QortalChatMessage msg in messages)
            {

                if (trackedMessages.Any(x => x.Signature == msg.Signature))
                {
                    continue;
                }
                else
                {
                    trackedMessages.Add(msg);

                    List<byte> first4Data = SimpleBase.Base58.Bitcoin.Decode(msg.Data).ToArray().ToList().GetRange(0, 4);
                    List<byte> first4Ref = SimpleBase.Base58.Bitcoin.Decode(msg.Reference).ToArray().ToList().GetRange(0, 4);

                    bool equal = first4Data.SequenceEqual(first4Ref);
                    
                    //check if this message is an encoded message // a message is to be considered as encodable if the first 4 bytes of the encoded message is the same as that of the first 4 bytes of the transaction reference
                    if (!equal)
                    {
                        continue;
                    }
                    
                    //if the request was sent my us
                    if (msg.Sender == _chatKeypair.Address)
                    {
                        LastmessageTime = DateTimeOffset.FromUnixTimeMilliseconds(msg.Timestamp);
                        //continue;
                    }

                    QByteLang.ProcessType pType = QByteDataMessageDataBuilder.GetType(msg);
                    
                    
                    switch (pType)
                    {
                        case QByteLang.ProcessType.DATAREQUEST:
                        {
                            
                            QbyteRequest qbr = new QbyteRequest(SimpleBase.Base58.Bitcoin.Decode(msg.Data).ToArray());
                            
                            //for now will just be a series of switch statements to figure out what calls need to be made.

                            //only handle configured coin code
                            if (qbr.CoinCode.ToString() != _infoHandle.NodeIndicator)
                            { 
                                //LastmessageTime = DateTimeOffset.FromUnixTimeMilliseconds(msg.Timestamp);
                                // return;
                            }

                            string infoNodeReturnData = null;

                            switch (qbr.RequestType)
                            {
                                case QByteLang.RequestType.GETBLOCK:
                                {
                                    switch (qbr.DataModifier)
                                    {
                                        case QByteLang.DataModifier.BYHASH:
                                        {
                                            infoNodeReturnData = await _infoHandle.GetBlock(SimpleBase.Base16.LowerCase.Encode(qbr.Data));
                                            break;
                                        }
                                        case QByteLang.DataModifier.BYHEIGHT:
                                        {
                                            infoNodeReturnData =
                                                await _infoHandle.GetBlock(BitConverter.ToInt32(qbr.Data));
                                            break;
                                        }
                                        case QByteLang.DataModifier.BYPART:
                                        default:
                                        {
                                            throw new ArgumentOutOfRangeException();
                                        }
                                    }
                                    break;
                                }
                                case QByteLang.RequestType.GETTRANSACTION:
                                {
                                    switch (qbr.DataModifier)
                                    {
                                        case QByteLang.DataModifier.BYHASH:
                                        {
                                            infoNodeReturnData = await _infoHandle.GetRawTransaction(SimpleBase.Base16.LowerCase.Encode(qbr.Data), 1);
                                            break;
                                        }
                                        case QByteLang.DataModifier.BYHEIGHT:
                                        case QByteLang.DataModifier.BYPART:
                                        default:
                                        {
                                            throw new ArgumentOutOfRangeException();
                                        }
                                    }
                                    break;
                                }

                                default:
                                {
                                    throw new ArgumentOutOfRangeException();
                                }
                            }

                            if (string.IsNullOrEmpty(infoNodeReturnData))
                            {
                                return;
                            }
                            
                            
                            QbyteReturnBuilder returnBuilder = new QbyteReturnBuilder();

                            returnBuilder.WithData(Encoding.UTF8.GetBytes(infoNodeReturnData));

                            QbyteReturn builtReturn = returnBuilder.Build(qbr.JobHash);

                            List<byte[]> chatMessagesData = builtReturn.Encode();

                            foreach (byte[] msgData in chatMessagesData)
                            {
                                bool isSent = await _qNodeHandle.SendChat(msgData, txMessageGroupId, _chatKeypair);

                                if (isSent = false)
                                {
                                    //
                                }
                            }
                            
                            break;
                        }
                        case QByteLang.ProcessType.DATARETURN:
                        {

                            List<byte> data = SimpleBase.Base58.Bitcoin.Decode(msg.Data).ToArray().ToList();
                            
                            bool partAdded = qCBuilder.TryAddPart(data, out QbyteReturnPart part);
                            
                            if (partAdded)
                            {
                                Console.WriteLine($"Message \"{msg.Data}\" contains part {(uint) part.PartNum}/{(uint) part.PartCount} for job: {part.JobHash}.");
                            }
                            else
                            {
                                if (part == null)
                                {
                                    Console.WriteLine($"Message \"{msg.Data}\" contains no decodable parts.");
                                }
                                else
                                {
                                    //when we already have the part tracked somehow???
                                }
                            }
                            break;
                        }
                        default:
                        {
                            Console.Write($"Could not decode processtype for {msg.Data}");
                            break;
                        }
                    }
                    LastmessageTime = DateTimeOffset.FromUnixTimeMilliseconds(msg.Timestamp);
                }
            }
        }
    }
}