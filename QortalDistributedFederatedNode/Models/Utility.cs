﻿using System;
using System.Collections.Generic;

namespace QortalDistributedFederatedNode.Models
{
    public static class Utility
    {
        public static List<List<T>> SplitChunks<T>(List<T> objects, int nSize = 5)
        {
            var list = new List<List<T>>();

            for (var i = 0; i<objects.Count;
                i += nSize) list.Add(objects.GetRange(i, Math.Min(nSize, objects.Count - i)));

            return list;
        }

    }
}