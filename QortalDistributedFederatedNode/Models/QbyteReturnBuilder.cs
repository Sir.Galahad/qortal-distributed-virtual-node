﻿﻿using System;
 using System.Collections.Generic;
 using System.IO;
 using System.Linq;
 using System.Security.Cryptography;
 using QortalDistributedFederatedNode.Models.Enums;

namespace QortalDistributedFederatedNode.Models
{
    public class QbyteReturnPart
    {
        private byte[] _jobHash;

        private byte[] _completeDataHash;

        private byte[] _dataHash;
        
        private byte _version;

        private byte _partNum;

        private byte _partCount;


        private QByteLang.ProcessType _processType = QByteLang.ProcessType.DATARETURN;

        private byte[] _data;

        public byte[] JobHash
        {
            get => _jobHash;
            private set => _jobHash = value;
        }

        public byte[] CompleteDataHash
        {
            get => _completeDataHash;
            private set => _completeDataHash = value;
        }

        public byte[] DataHash
        {
            get => _dataHash;
            private set => _dataHash = value;
        }

        public byte PartNum
        {
            get => _partNum;
            private set => _partNum = value;
        }

        public byte PartCount
        {
            get => _partCount;
            private set => _partCount = value;
        }

        public byte[] Data
        {
            get => _data;
            private set => _data = value;
        }

        public QbyteReturnPart Decode(List<byte> Encoded)
        {

            int depth = 0;
            
            depth += 4;

            _version = Encoded[depth];

            depth += 1;

            _processType = (QByteLang.ProcessType) Encoded[depth]; 

            depth += 1;
            
            _jobHash = Encoded.GetRange(depth, 4).ToArray();
            
            depth += 4;

            _completeDataHash = Encoded.GetRange(depth, 4).ToArray(); 

            depth += 4;

            _dataHash = Encoded.GetRange(depth, 4).ToArray(); 

            depth += 4;

            _partNum = Encoded[depth];
            
            depth += 1;
            
            _partCount = Encoded[depth];

            depth += 1;

            _data = Encoded.Skip(depth).ToArray();

            return this;
        }
    }

    public class QbyteReturn
    {

        private byte[] _jobHash;
        
        private byte _version;

        private QByteLang.ProcessType _processType = QByteLang.ProcessType.DATARETURN;

        public byte[] _data;
        
        public byte[] JobHash
        {
            get => _jobHash;
            private set => _jobHash = value;
        }

        public QbyteReturn(byte[] jobHash, byte version, byte[] data)
        {
            _jobHash = jobHash;
            _version = version;
            _data = data;
        }

        public List<byte[]> Encode()
        {
            int txSize = 256;
            
            int infoLegnth = 0;

            infoLegnth += 4; //reference
            
            infoLegnth += 1; //version
  
            infoLegnth += 1; //type             

            infoLegnth += 4; //jobhash

            infoLegnth += 4; //CompletedataHash

            MemoryStream infoByteCode = new MemoryStream(infoLegnth * 8);
            
            BinaryWriter infoWriter = new BinaryWriter(infoByteCode);

            infoWriter.Seek(4, SeekOrigin.Begin); // 4

            infoWriter.Write(_version); // 5
                 
            infoWriter.Write((byte) _processType); // 6       

            infoWriter.Write(_jobHash); //10

            byte[] compDataHash = SHA256.Create().ComputeHash(_data).Take(4).ToArray();
            
            infoWriter.Write(compDataHash); //14
            
            
            long maxDataSize = txSize - (infoByteCode.Length + 6); // datahash +4 partinfo +2
            

            List<List<byte>> dataChunks = Utility.SplitChunks(_data.ToList(), (int) maxDataSize);
            
            List<byte[]> encodedReturns = new List<byte[]>();

            for (int i = 0; i < dataChunks.Count; i++)
            {

                MemoryStream encodedByteCode = new MemoryStream((int) (infoByteCode.Length + dataChunks[i].Count() * 8));
                
                BinaryWriter writer = new BinaryWriter(encodedByteCode);
                
                writer.Write(infoByteCode.ToArray());

                writer.Write(SHA256.Create().ComputeHash(dataChunks[i].ToArray()).Take(4).ToArray());

                writer.Write((byte) i);

                byte chunkcount = (byte) (dataChunks.Count - 1);
                
                writer.Write(chunkcount);

                byte[] ar = dataChunks[i].ToArray();
                
                writer.Write(ar);
                
                encodedReturns.Add(encodedByteCode.ToArray());
            }

            return encodedReturns;
        }
    }
    
    public class QbyteReturnBuilder
    {
     
        private QByteLang.ProcessType _processType = QByteLang.ProcessType.DATARETURN;
        
        private byte _version = 0x01;
        
        private byte[] _data;

        public byte[] Data
        {
            get => _data;
            private set => _data = value;
        }
        
        public QByteLang.ProcessType ProcessType
        {
            get => _processType;
            private set => _processType = value;
        }
        
        
        public QbyteReturnBuilder WithData(byte[] data)
        {
            _data = data;
            return this;
        }

        public QbyteReturn Build(byte[] jobhash)
        {
            return new QbyteReturn(jobhash, _version, _data);
        }

        public List<byte[]> Encoded(byte[] jobhash)
        {
            return Build(jobhash).Encode();
        }

    }
}