﻿﻿using System.Collections.Generic;

namespace QortalDistributedFederatedNode.Models.Enums
{
    public static class QByteLang
    {

        //request: reference(4) version(1) processType(1) jobHash(4) CoinCode(2) requestType(1) DataModifier(1) data
        
        //return: reference(4) version(1) processType(1) jobHash(4) CompleteDataHash(4) DataPartHash(4) PartNum(1) PartCount(1) Data
        
        
        public enum Coin //next 2 bytes
        {
            BITCOIN = 0x00,
            LITECOIN = 0x01
        }

        public enum ProcessType //next byte
        {
            DATAREQUEST = 0x00,
            DATARETURN = 0x01,
        }

        public enum RequestType //next byte, requires a following modifier
        {
            GETBLOCK = 0x00,
            GETTRANSACTION = 0x01,
            BROADCASTTRANSACTION = 0x02
        }

        public enum DataModifier //next byte
        {
            BYHASH = 0x00, //next 32 bytes

            BYHEIGHT =
                0x01, //next 4 bytes //i figure that there wont be any blockchains that reasonably have 4 billion blocks in my lifetime
            BYPART = 0x02
        }
        
        public static Dictionary<DataModifier, int> ModifierLegnths = new Dictionary<DataModifier, int>()
        {
            {DataModifier.BYHASH, 32},
            {DataModifier.BYHEIGHT, 4}
        };

        public enum ReturnDataType //next byte
        {
            BLOCKDATA = 0x00,
            TRANSACTIONDATA = 0x01
        }
        
        
        
    }
}