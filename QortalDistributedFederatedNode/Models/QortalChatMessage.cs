﻿﻿using System.Text.Json.Serialization;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalChatMessage
    {
        [JsonPropertyName("timestamp")]
        public long Timestamp { get; set; } 

        [JsonPropertyName("txGroupId")]
        public int TxGroupId { get; set; } 

        [JsonPropertyName("reference")]
        public string Reference { get; set; } 

        [JsonPropertyName("senderPublicKey")]
        public string SenderPublicKey { get; set; } 

        [JsonPropertyName("sender")]
        public string Sender { get; set; } 

        [JsonPropertyName("senderName")]
        public string SenderName { get; set; } 

        [JsonPropertyName("data")]
        public string Data { get; set; } 

        [JsonPropertyName("isText")]
        public bool IsText { get; set; } 

        [JsonPropertyName("isEncrypted")]
        public bool IsEncrypted { get; set; } 

        [JsonPropertyName("signature")]
        public string Signature { get; set; } 
    }
}