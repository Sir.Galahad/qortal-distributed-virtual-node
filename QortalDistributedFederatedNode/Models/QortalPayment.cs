﻿﻿using System;
using Newtonsoft.Json;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalPayment
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("timestamp")] public long Timestamp { get; set; }

        [JsonProperty("reference")] public string Reference { get; set; }

        [JsonProperty("fee")] public Decimal Fee { get; set; }

        [JsonProperty("signature")] public string Signature { get; set; }

        [JsonProperty("txGroupId")] public int TxGroupId { get; set; }

        [JsonProperty("blockHeight")] public int BlockHeight { get; set; }

        [JsonProperty("approvalStatus")] public string ApprovalStatus { get; set; }

        [JsonProperty("creatorAddress")] public string CreatorAddress { get; set; }

        [JsonProperty("senderPublicKey")] public string SenderPublicKey { get; set; }

        [JsonProperty("recipient")] public string Recipient { get; set; }

        [JsonProperty("amount")] public decimal Amount { get; set; }

    }
}