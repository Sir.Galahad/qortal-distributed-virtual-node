﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace QortalDistributedFederatedNode.Models
{
    public class AddressKeyPair
    {
        public string Address { get; set; }
        
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
        
    }
}