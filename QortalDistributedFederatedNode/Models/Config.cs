﻿﻿using System;
 using System.Collections.Generic;

 namespace QortalDistributedFederatedNode.Models
{
    public class Config
    {
        public NodeRPC InfoNode { get; set; } = new NodeRPC();

        public NodeRPC QortalNode { get; set; } = new NodeRPC();

        public string QortalChatPrivateKey { get; set; } = "";

    }

    public class NodeRPC
    {
        public string NodeIndicator { get; set; } = "";

        public string NodeIp { get; set; } = "";
        public string NodePort { get; set; } = "";
        public string NodeUsername { get; set; } = "";
        public string NodePassword { get; set; } = "";
    }
}