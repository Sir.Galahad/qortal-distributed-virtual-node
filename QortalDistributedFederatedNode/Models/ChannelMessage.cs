﻿﻿namespace QortalDistributedFederatedNode.Models
{
    public class ChannelMessage
    {

        public string CoinName { get; set; }
        public string SignedMessageHex { get; set; }
    }
}