﻿﻿using Newtonsoft.Json;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalBlockchainResult
    {
        [JsonProperty("signature")]
        public string Signature { get; set; } 

        [JsonProperty("version")]
        public int Version { get; set; } 

        [JsonProperty("reference")]
        public string Reference { get; set; } 

        [JsonProperty("transactionCount")]
        public int TransactionCount { get; set; } 

        [JsonProperty("totalFees")]
        public string TotalFees { get; set; } 

        [JsonProperty("transactionsSignature")]
        public string TransactionsSignature { get; set; } 

        [JsonProperty("height")]
        public int Height { get; set; } 

        [JsonProperty("timestamp")]
        public long Timestamp { get; set; } 

        [JsonProperty("minterPublicKey")]
        public string MinterPublicKey { get; set; } 

        [JsonProperty("minterSignature")]
        public string MinterSignature { get; set; } 

        [JsonProperty("atCount")]
        public int AtCount { get; set; } 

        [JsonProperty("atFees")]
        public string AtFees { get; set; } 

        [JsonProperty("encodedOnlineAccounts")]
        public string EncodedOnlineAccounts { get; set; } 

        [JsonProperty("onlineAccountsCount")]
        public int OnlineAccountsCount { get; set; } 

        [JsonProperty("onlineAccountsTimestamp")]
        public long OnlineAccountsTimestamp { get; set; } 

        [JsonProperty("onlineAccountsSignatures")]
        public string OnlineAccountsSignatures { get; set; } 

        [JsonProperty("minterAddress")]
        public string MinterAddress { get; set; } 
    }
}