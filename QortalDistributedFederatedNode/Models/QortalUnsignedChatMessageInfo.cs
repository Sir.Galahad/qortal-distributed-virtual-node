﻿using System;
using Newtonsoft.Json;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalUnsignedChatMessageInfo
    {
        [JsonProperty("timestamp")]
        public long Timestamp { get; set; } 

        [JsonProperty("reference")]
        public string Reference { get; set; } 

        [JsonProperty("fee")]
        public decimal Fee { get; set; } 

        [JsonProperty("txGroupId")]
        public int TxGroupId { get; set; } 

        [JsonProperty("senderPublicKey")]
        public string SenderPublicKey { get; set; } 

        [JsonProperty("recipient")]
        public string Recipient { get; set; } 

        [JsonProperty("data")]
        public string Data { get; set; } 

        [JsonProperty("isText")]
        public bool IsText { get; set; } 

        [JsonProperty("isEncrypted")]
        public bool IsEncrypted { get; set; }
    }
}