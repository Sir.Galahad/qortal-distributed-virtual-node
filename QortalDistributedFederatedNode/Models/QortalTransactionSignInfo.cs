﻿﻿using Newtonsoft.Json;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalTransactionSignInfo
    {
        [JsonProperty("privateKey")]
        public string PrivateKey { get; set; } 

        [JsonProperty("transactionBytes")]
        public string TransactionBytes { get; set; } 
    }
}