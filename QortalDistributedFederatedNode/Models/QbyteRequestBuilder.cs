﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using QortalDistributedFederatedNode.Models.Enums;

namespace QortalDistributedFederatedNode.Models
{
    public class QbyteRequest
    {
        public QbyteRequest(byte[] Encoded)
        {

            Decode(Encoded.ToList());
        }

        public QbyteRequest(QByteLang.Coin coinCode, byte version, QByteLang.ProcessType processType,
            QByteLang.RequestType requestType, QByteLang.DataModifier dataModifier, int modifierLength, byte[] data)
        {
            _coinCode = coinCode;
            _version = version;
            _processType = processType;
            _requestType = requestType;
            _dataModifier = dataModifier;
            _modifierLength = modifierLength;
            _data = data;
            
        }

        private byte[] _jobHash;
        private QByteLang.Coin _coinCode;
        private byte _version = 0x01;
        private QByteLang.ProcessType _processType = QByteLang.ProcessType.DATAREQUEST;
        private QByteLang.RequestType _requestType;
        private QByteLang.DataModifier _dataModifier;
        
        private byte[] _data;
        
        private int _modifierLength;
        
        public QByteLang.Coin CoinCode
        {
            get => _coinCode;
            private set => _coinCode = value;
        }

        public byte[] JobHash
        {
            get => _jobHash;
            private set => _jobHash = value;
        }

        public QByteLang.ProcessType ProcessType
        {
            get => _processType;
            private set => _processType = value;
        }
        
        public QByteLang.RequestType RequestType
        {
            get => _requestType;
            private set => _requestType = value;
        }

        public QByteLang.DataModifier DataModifier
        {
            get => _dataModifier;
            private set => _dataModifier = value;
        }
        
        public byte[] Data
        {
            get => _data;
            private set => _data = value;
        }

        public QbyteRequest Decode(List<byte> Encoded)
        {

            int depth = 0;

            depth += 4; //skip reference

            _version = Encoded[depth];

            depth += 1;

            byte procType = Encoded[depth];
            
            ProcessType = (QByteLang.ProcessType) procType;

            depth += 1;

            _jobHash = Encoded.GetRange(depth, 4).ToArray();
            
            depth += 4;
            
            byte[] coinCode = Encoded.GetRange(depth, 2).ToArray();
            
            CoinCode = (QByteLang.Coin) BitConverter.ToUInt16(coinCode.Reverse().ToArray());
            
            depth += 2;

            RequestType = (QByteLang.RequestType) Encoded[depth];

            depth += 1;

            DataModifier = (QByteLang.DataModifier) Encoded[depth];

            depth += 1;

            Data = Encoded.Skip(depth).ToArray();

            return this;
        }
        
        public byte[] Encode()
        {
            int length = 0;

            length += 4; //reference
            
            length++; // add 1 byte for message version

            length++; // add 1 byte for process type

            length += 4; //jobhash
            
            length += 2; // add 2 byte for coin type length

            length++; // add 1 byte for request type

            length++; // add 1 byte for modifier

            switch (DataModifier)
            {
                case QByteLang.DataModifier.BYHASH:
                {
                    length += 32;
                    break;
                }
                case QByteLang.DataModifier.BYHEIGHT:
                {
                    length += 4;
                    break;
                }
                case QByteLang.DataModifier.BYPART:
                {
                    length += 2;
                    break;
                }
                default:
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
            
            
            MemoryStream byteCode = new MemoryStream(length * 8);
            
            BinaryWriter writer = new BinaryWriter(byteCode);

            writer.Seek(4, SeekOrigin.Begin); // skip first 32 bits of reference
            
            writer.Write(_version); //one byte version
            
            writer.Write((byte) ProcessType); // on byte process

            writer.Seek(10, SeekOrigin.Begin); // skip 32 bit job hash
            
            byte[] coinBytes = BitConverter.GetBytes((Int16) _coinCode); //2 byte coin code

            writer.Write(coinBytes.Reverse().ToArray());
            
            writer.Write((byte) RequestType);

            writer.Write((byte) DataModifier);

            //this is for possible passing data by part in the future.
            if (DataModifier == QByteLang.DataModifier.BYPART)
            {
                //writer.Write(Convert.ToByte(_partNum));
                //writer.Write(Convert.ToByte(_parts));
            }
            
            writer.Write(Data);

            byte[] scriptHash = SHA256.Create().ComputeHash(byteCode.ToArray().Skip(4).ToArray()).Take(4).ToArray();

            writer.Seek(6, SeekOrigin.Begin);
            
            writer.Write(scriptHash.Take(4).ToArray());

            return byteCode.ToArray();
        }
        
        
    }
    
    
    
    
    public class QbyteRequestBuilder
    {
        private QByteLang.Coin _coinCode;
        private byte _version = 0x01;
        private QByteLang.ProcessType _processType = QByteLang.ProcessType.DATAREQUEST;
        private QByteLang.RequestType _requestType;
        private QByteLang.DataModifier _dataModifier;

        private byte[] _data;
        
        private int modifierLength;


        public QByteLang.Coin CoinCode
        {
            get => _coinCode;
            private set => _coinCode = value;
        }

        public QByteLang.ProcessType ProcessType
        {
            get => _processType;
            private set => _processType = value;
        }
        
        public QByteLang.RequestType RequestType
        {
            get => _requestType;
            private set => _requestType = value;
        }

        public QByteLang.DataModifier DataModifier
        {
            get => _dataModifier;
            private set => _dataModifier = value;
        }
        
        public byte[] Data
        {
            get => _data;
            private set => _data = value;
        }

        
        
        public QbyteRequestBuilder ForCoin(QByteLang.Coin coinCode)
        {
            CoinCode = coinCode;

            return this;
        }
        
        public QbyteRequestBuilder WithRequest(QByteLang.RequestType requestType)
        {
            RequestType = requestType;

            return this;
        }

        public QbyteRequestBuilder WithDataModifier(QByteLang.DataModifier modifier)
        {
            DataModifier = modifier;

            return this;
        }

        public QbyteRequestBuilder WithData(byte[] data)
        {
            
            
            modifierLength = QByteLang.ModifierLegnths[_dataModifier];

            if (modifierLength != data.Length)
            {
                throw new InvalidDataException("Data size must be equal to the modifier legnth.");
            }

            _data = data;

            return this;
        }

        public QbyteRequest Build()
        {
            return new QbyteRequest(CoinCode, _version, ProcessType, RequestType, DataModifier, modifierLength, Data);
        }

        public byte[] Encode()
        {
            return Build().Encode();
        }
    }
}