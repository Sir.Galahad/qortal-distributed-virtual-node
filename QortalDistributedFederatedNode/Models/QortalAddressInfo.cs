﻿﻿using Newtonsoft.Json;

namespace QortalDistributedFederatedNode.Models
{
    public class QortalAddressInfo
    {

        [JsonProperty("address")] public string Address { get; set; }

        [JsonProperty("reference")] public string Reference { get; set; }

        [JsonProperty("publicKey")] public string PublicKey { get; set; }

        [JsonProperty("defaultGroupId")] public int DefaultGroupId { get; set; }

        [JsonProperty("flags")] public int Flags { get; set; }

        [JsonProperty("level")] public int Level { get; set; }

        [JsonProperty("blocksMinted")] public int BlocksMinted { get; set; }

        [JsonProperty("blocksMintedAdjustment")]
        public int BlocksMintedAdjustment { get; set; }
        
    }
}